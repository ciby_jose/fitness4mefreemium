//
//  CustomizedWorkoutPlayViewController.m
//  Fitness4Me
//
//  Created by Ciby  on 06/12/12.
//
//

#import "CustomizedWorkoutPlayViewController.h"

@interface CustomizedWorkoutPlayViewController ()

@end

@implementation CustomizedWorkoutPlayViewController

@synthesize excersices,WorkoutID,workout,userID;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:[Fitness4MeUtils getBundle]];
    if (self) {
        // Custom initialization
    }
    return self;
}

static int initalArrayCount=0;
static int playCount=0;
static float totalDuration=0;

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self getExcersices];
    self.view.transform = CGAffineTransformConcat(self.view.transform, CGAffineTransformMakeRotation(M_PI_2));
    screenBounds = [[UIScreen mainScreen] bounds];
    
    if (screenBounds.size.height == 568)
    {
        [subview5 removeFromSuperview];
        [self.view addSubview:subview];
    }
    else{
        [subview5 removeFromSuperview];
        [self.view addSubview:subview];
    }
    
    moviePlayer.view.backgroundColor =[UIColor  whiteColor];
    [self showAdMobs];
    
    
}


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    totalDuration=0;
}


-(void)showAdMobs
{
    NSUserDefaults *userinfo =[NSUserDefaults standardUserDefaults];
    NSString *hasMadeFullPurchase= [userinfo valueForKey:@"hasMadeFullPurchase"];
    [userinfo setObject:@"false" forKey:@"shouldExit"];
    if ([hasMadeFullPurchase isEqualToString:@"true"]) {
        
    }
    else {
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone){
            bannerView_ = [[GADBannerView alloc]
                           initWithFrame:CGRectMake(0,-7,
                                                    self.view.frame.size.width-70,
                                                    50)];
        }else {
            bannerView_ = [[GADBannerView alloc]
                           initWithFrame:CGRectMake(0,0,
                                                    self.view.frame.size.height-70,
                                                    90)];
        }
        
        bannerView_.adUnitID = @"a150efb4cbe1a0a";
        bannerView_.rootViewController = self;
        [self.view addSubview:bannerView_];
        // Initiate a generic request to load it with an ad.
        [bannerView_ loadRequest:[GADRequest request]];
    }
}

-(void)getExcersices
{
    
    NSUserDefaults *userinfo =[NSUserDefaults standardUserDefaults];
    self.workoutType =[userinfo stringForKey:@"workoutType"];
    excersiceDB =[[ExcersiceDB alloc]init];
    [excersiceDB setUpDatabase];
    [excersiceDB createDatabase];
    if ([self.workoutType isEqualToString:@"Custom"])
        [excersiceDB getCustomExcersices:WorkoutID];
    else
        [excersiceDB getSelfMadeExcersices:WorkoutID];
    arr = [[NSMutableArray alloc]init];
    arr =excersiceDB.Excersices;
    [self MakeArrayforViewing ];
    [self initializPlayer ];
}

-(void)MakeArrayforViewing
{
    
    ExcersicePlay *play;
    aras =[[NSMutableArray alloc]init];
    for (int i=0; i<[arr count]; i++) {
        play =[[ExcersicePlay alloc]init];
        play.repeatIntervel =[[[arr objectAtIndex:i]PosterRepeatCount]intValue];
        play.videoName =[[arr objectAtIndex:i] PosterName];
        if ([play.videoName length]>0) {
            [aras addObject:play];
        }
        
        play =[[ExcersicePlay alloc]init];
        play.repeatIntervel =[[[arr objectAtIndex:i]RepeatCount]intValue];
        play.videoName =[[arr objectAtIndex:i] Name];
        if ([play.videoName length]>0) {
            [aras addObject:play];
        }
        
        play =[[ExcersicePlay alloc]init];
        play.repeatIntervel =[[[arr objectAtIndex:i]StopRep] intValue];
        play.videoName =[[arr objectAtIndex:i] StopName];
        if ([play.videoName length]>0) {
            [aras addObject:play];
        }
        
        play =[[ExcersicePlay alloc]init];
        play.repeatIntervel =[[[arr objectAtIndex:i]OthersidePosterRep]intValue];
        play.videoName =[[arr objectAtIndex:i] OthersidePosterName];
        if ([play.videoName length]>0) {
            [aras addObject:play];
        }
        
        play =[[ExcersicePlay alloc]init];
        play.repeatIntervel =[[[arr objectAtIndex:i]OthersideRep]intValue];
        play.videoName =[[arr objectAtIndex:i] OthersideName];
        if ([play.videoName length]>0) {
            [aras addObject:play];
        }
        
        play =[[ExcersicePlay alloc]init];
        play.repeatIntervel =1;
        play.videoName =[[arr objectAtIndex:i] RecoveryVideoName];
        if ([play.videoName length]>0) {
            [aras addObject:play];
        }
        
        play =[[ExcersicePlay alloc]init];
        play.repeatIntervel =[[[arr objectAtIndex:i]NextRep]intValue];
        play.videoName =[[arr objectAtIndex:i] NextName];
        if ([play.videoName length]>0) {
            [aras addObject:play];
        }
        
        play =[[ExcersicePlay alloc]init];
        play.repeatIntervel =[[[arr objectAtIndex:i]CompletedRep]intValue];
        play.videoName =[[arr objectAtIndex:i] CompletedName];
        if ([play.videoName length]>0) {
            [aras addObject:play];
        }
    }
}



-(void)initializPlayer
{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"MyFolder"];
    storeURL =  [dataPath stringByAppendingPathComponent :[[aras objectAtIndex:initalArrayCount]videoName]];
    if (playCount==0) {
        if(initalArrayCount<[aras count]){
            initalArrayCount +=1;
        }else {
            initalArrayCount = 0;
        }
    }
    if (![[NSFileManager defaultManager] fileExistsAtPath:storeURL]){
        if (initalArrayCount<[aras count]) {
            int count =[[aras objectAtIndex:initalArrayCount-1]repeatIntervel];
            if (count==0) {
                count=1;
            }
            if (playCount==count) {
                playCount=0;
            }
            [self initializPlayer];
        }
    }else {
        if (moviePlayer!=nil) {
            if (playCount==0) {
                moviePlayer.view .frame= subview.bounds;
                moviePlayer.contentURL =[NSURL fileURLWithPath:storeURL];
                moviePlayer.controlStyle = MPMovieControlStyleNone;
                [moviePlayer play];
            }else {
                [moviePlayer play];
                moviePlayer.controlStyle = MPMovieControlStyleNone;
            }
        }else {
            moviePlayer = [[MPMoviePlayerController alloc] init];
            moviePlayer.contentURL =[NSURL fileURLWithPath:storeURL];
            [moviePlayer play];
            moviePlayer.controlStyle = MPMovieControlStyleNone;
            moviePlayer.view .frame= subview.bounds;
            [subview addSubview: moviePlayer.view];
        }
        
        moviePlayer.controlStyle = MPMovieControlStyleNone;
        //Register to receive a notification when the movie has finished playing.
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(moviePlayBackDidFinish:)
                                                     name:MPMoviePlayerPlaybackDidFinishNotification
                                                   object:moviePlayer];
    }
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    NSUInteger numTaps = [[touches anyObject] tapCount];
    if (numTaps == 1){
        if(moviePlayer.controlStyle == MPMovieControlStyleEmbedded)
            moviePlayer.controlStyle = MPMovieControlStyleNone;
        else
            moviePlayer.controlStyle = MPMovieControlModeVolumeOnly;
    }
}

-(void)showAlert
{
    
    
    NSString * cancelText;
    
    if ([Fitness4MeUtils getApplicationLanguage] ==1) {
        cancelText =@"Cancel";
    }
    else
    {
        cancelText =@"ablehnen";
    }

    
    UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"fitness4.me" message:NSLocalizedStringWithDefaultValue(@"exitVideo", nil,[Fitness4MeUtils getBundle], nil, nil)
                                                       delegate:self cancelButtonTitle:@"OK" otherButtonTitles:cancelText, nil];
    [alertview show];
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.0];
    
    alertview.transform = CGAffineTransformRotate(alertview.transform, 3.14159/2);
    [UIView commitAnimations];
    
    
}

- (void)didPresentAlertView:(UIAlertView *)alertView
{
    // UIAlertView in landscape mode
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.0];
    alertView.transform = CGAffineTransformRotate(alertView.transform, 3.14159/2);
    [UIView commitAnimations];
}

- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==0) {
        if (moviePlayer!=nil) {
            totalDuration+= [moviePlayer currentPlaybackTime];
            totalDuration =totalDuration;
            [self updateStatisticsToServer];
            
        }
        initalArrayCount=0;
        playCount=0;
        moviePlayer=nil;
        CustomWorkoutsViewController *viewController;
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone){
            viewController =[[CustomWorkoutsViewController alloc]initWithNibName:@"CustomWorkoutsViewController" bundle:nil];
        }else {
            viewController =[[CustomWorkoutsViewController alloc]initWithNibName:@"CustomWorkoutsViewController_iPad" bundle:nil];
        }
        
        viewController.workoutType =self.workoutType;
        [self.navigationController pushViewController:viewController animated:YES];
        
    }
}

-(IBAction)onClickBack:(id)sender{
    [self showAlert];
}


- (void) moviePlayBackDidFinish:(NSNotification*)notification {
    moviePlayer = [notification object];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:MPMoviePlayerPlaybackDidFinishNotification
                                                  object:moviePlayer];
    
    playCount=playCount+1;
    if (initalArrayCount<[aras count]) {
        int count =[[aras objectAtIndex:initalArrayCount-1]repeatIntervel];
        if (count==0) {
            count=1;
        }
        if (playCount==count) {
            playCount=0;
        }
        
        [self initializPlayer];
        
        totalDuration+= [moviePlayer currentPlaybackTime];
    }
    else {
        
        initalArrayCount=0;
        playCount=0;
        totalDuration+= [moviePlayer currentPlaybackTime];
        totalDuration=totalDuration;
        [self updateStatisticsToServer];
        
        CustomWokoutPostplayViewController *viewController;
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone){
            viewController = [[CustomWokoutPostplayViewController alloc]initWithNibName:@"CustomWokoutPostplayViewController" bundle:nil];
        }
        else {
            viewController = [[CustomWokoutPostplayViewController alloc]initWithNibName:@"CustomWokoutPostplayViewController_iPad" bundle:nil];
        }
        [viewController setWorkoutType:self.workoutType];
        viewController.workout =self.workout;
        [self.navigationController pushViewController:viewController animated:YES];
        
    }
}


-(void)updateStatisticsToServer
{
    
    NSUserDefaults *userinfo =[NSUserDefaults standardUserDefaults];
    NSString* workoutType = [userinfo stringForKey:@"workoutType"];
    BOOL isReachable = [Fitness4MeUtils isReachable];
    if (isReachable) {
        NSString *requestString;
        NSString *UrlPath= [NSString GetURlPath];
        
        
        if ([workoutType isEqualToString:@"SelfMade"]){
            requestString= [NSString stringWithFormat:@"%@selfstats=yes&userid=%@&workoutid=%i&duration=%f",UrlPath,userID,WorkoutID,totalDuration];
            
        }
        
        else{
            requestString= [NSString stringWithFormat:@"%@customstats=yes&userid=%@&workoutid=%i&duration=%f",UrlPath,userID,WorkoutID,totalDuration];
        }
        
        
        NSURL *url =[NSURL URLWithString:[requestString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        __block ASIHTTPRequest *requests = [ASIHTTPRequest requestWithURL:url];
        [requests setCompletionBlock:^{
            
        }];
        [requests setFailedBlock:^{
            
        }];
        [requests startAsynchronous];
    }else{
        
        statisticsDB =[[StatisticsDB alloc]init];
        [statisticsDB setUpDatabase];
        [statisticsDB createDatabase];
        Statistics *statistics =[[Statistics alloc]init];
        [statistics setDuration:totalDuration];
        [statistics setWorkoutID:[NSString stringWithFormat:@"%i",WorkoutID]];
        if ([workoutType isEqualToString:@"SelfMade"]){
            [statisticsDB insertSelfMadeStatistics:statistics];
        }
        else{
            [statisticsDB insertCustomStatistics:statistics];
        }
    }
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    moviePlayer=nil;
    arr=nil;
    aras=nil;
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(BOOL)shouldAutorotate {
    return NO;
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft);
}


@end
