//
//  Equipments.h
//  Fitness4Me
//
//  Created by Ciby  on 03/12/12.
//
//

#import <Foundation/Foundation.h>

@interface Equipments : NSObject
@property(strong,nonatomic)NSString *equipmentID;
@property(strong, nonatomic)NSString *equipmentName;
@property(nonatomic)BOOL isChecked;
@end
