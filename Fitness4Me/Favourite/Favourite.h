//
//  Favourite.h
//  Fitness4Me
//
//  Created by Ciby  on 17/12/12.
//
//

#import <Foundation/Foundation.h>

@interface Favourite : NSObject

@property(nonatomic,retain) NSString *workoutID;
@property(nonatomic)int status;
@end
