//
//  Statistics.h
//  Fitness4Me
//
//  Created by Ciby on 21/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Statistics : NSObject
@property (nonatomic,retain) NSString *WorkoutID;
@property (nonatomic) float Duration;
@end
