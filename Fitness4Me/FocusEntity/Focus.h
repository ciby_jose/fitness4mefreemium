//
//  Focus.h
//  Fitness4Me
//
//  Created by Ciby  on 04/12/12.
//
//

#import <Foundation/Foundation.h>

@interface Focus : NSObject
@property(strong,nonatomic)NSString *muscleID;
@property(strong, nonatomic)NSString *muscleName;
@property(nonatomic)BOOL isChecked;
@end
